<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/retrieve', function () {

$xml=simplexml_load_file("product_list.xml") or die("Error: Cannot create object");
$xmlDoc = new DOMDocument();
$xmlDoc->load("product_list.xml");
$products_e = [];
foreach($xmlDoc->documentElement->childNodes  AS $item)
{
    if($item->nodeName == 'product')
    {
        $products_e[] = [
            'product_name' =>$item->childNodes[1]->nodeValue,
            'qty_in_stock' =>$item->childNodes[3]->nodeValue,
            'price_per_item' =>$item->childNodes[5]->nodeValue
        ];

        echo $item->childNodes[1]->nodeName . " = " . $item->childNodes[1]->nodeValue . "<br>";
    }
    

}
$products_e[] = [
    'product_name' => 'ProdEr',
    'qty_in_stock' => '18',
    'price_per_item' => '78',
];

$dom = new DOMDocument();

		$dom->encoding = 'utf-8';

		$dom->xmlVersion = '1.0';

		$dom->formatOutput = true;

	$xml_file_name = 'product_list.xml';

        $root = $dom->createElement('products');
        foreach($products_e as $d)
        {
            $product_node = $dom->createElement('product'); 
            
            $child_node_title = $dom->createElement('product_name', $d['product_name']);
            $product_node->appendChild($child_node_title);
            $child_node_title = $dom->createElement('qty_in_stock', $d['qty_in_stock']);
            $product_node->appendChild($child_node_title);
            $child_node_title = $dom->createElement('price_per_item', $d['price_per_item']);
            $product_node->appendChild($child_node_title);
            $root->appendChild($product_node);
        }
        

		$dom->appendChild($root);

});
Route::resource('/products', 'productController');

Route::get('/check', function () {
    $products_e = [
        [
            'product_name' => 'Prod1',
            'qty_in_stock' => '15',
            'price_per_item' => '34',
        ],
        [
            'product_name' => 'Prod2',
            'qty_in_stock' => '20',
            'price_per_item' => '40',
        ],
    ];
    $dom = new DOMDocument();

		$dom->encoding = 'utf-8';

		$dom->xmlVersion = '1.0';

		$dom->formatOutput = true;

	$xml_file_name = 'product_list.xml';

        $root = $dom->createElement('products');
        foreach($products_e as $d)
        {
            $product_node = $dom->createElement('product');
            


            $child_node_title = $dom->createElement('product_name', $d['product_name']);
            $product_node->appendChild($child_node_title);
            $child_node_title = $dom->createElement('qty_in_stock', $d['qty_in_stock']);
            $product_node->appendChild($child_node_title);
            $child_node_title = $dom->createElement('price_per_item', $d['price_per_item']);
            $product_node->appendChild($child_node_title);
            $root->appendChild($product_node);
        }


		$dom->appendChild($root);

	$dom->save($xml_file_name);
});
