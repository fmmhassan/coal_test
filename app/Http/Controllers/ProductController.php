<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $xml=simplexml_load_file("product_list.xml") or die("Error: Cannot create object");
        $xmlDoc = new \DOMDocument();
        $xmlDoc->load("product_list.xml");
        $products_e = [];
        foreach($xmlDoc->documentElement->childNodes  AS $item)
        {
            if($item->nodeName == 'product')
            {
                $products_e[] = [
                    'product_name' =>$item->childNodes[1]->nodeValue,
                    'qty_in_stock' =>$item->childNodes[3]->nodeValue,
                    'price_per_item' =>$item->childNodes[5]->nodeValue
                ];
            }
        }
        return response()->json($products_e);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'qty_in_stock' => 'required|numeric',
            'price_per_item' => 'required|numeric',
        ]);
        
        $xml=simplexml_load_file("product_list.xml") or die("Error: Cannot create object");
        $xmlDoc = new \DOMDocument();
        $xmlDoc->load("product_list.xml");
        $products_e = [];
        foreach($xmlDoc->documentElement->childNodes  AS $item)
        {
            if($item->nodeName == 'product')
            {
                $products_e[] = [
                    'product_name' =>$item->childNodes[1]->nodeValue,
                    'qty_in_stock' =>$item->childNodes[3]->nodeValue,
                    'price_per_item' =>$item->childNodes[5]->nodeValue
                ];
            }
        }
        $products_e[] = [
            'product_name' => $request->product_name,
            'qty_in_stock' => $request->qty_in_stock,
            'price_per_item' => $request->price_per_item,
        ];
        $dom = new \DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->xmlVersion = '1.0';

        $dom->formatOutput = true;

        $xml_file_name = 'product_list.xml';

        $root = $dom->createElement('products');
        foreach($products_e as $d)
        {
            $product_node = $dom->createElement('product'); 
            
            $child_node_title = $dom->createElement('product_name', $d['product_name']);
            $product_node->appendChild($child_node_title);
            $child_node_title = $dom->createElement('qty_in_stock', $d['qty_in_stock']);
            $product_node->appendChild($child_node_title);
            $child_node_title = $dom->createElement('price_per_item', $d['price_per_item']);
            $product_node->appendChild($child_node_title);
            $root->appendChild($product_node);
        }

        

        $dom->appendChild($root);
        $dom->save($xml_file_name);
        return response()->json(true);
    }

}
