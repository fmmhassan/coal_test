<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
        </script>
    </head>
    <body>
    <div class="row">
            <div class="col-md-12">
                <div class="card " >
                    <div class="card-body">
                        <h3>Post</h3>
                        <form id="product_form">
                            <div style="color: #ff5252;" id="error_message"></div>
                            <div class="text-success" id="success_message"></div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Product Name</label>
                                        <input type="text" class="form-control" name="product_name" id="product_name" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Quantity in stock</label>
                                        <input type="text" class="form-control" name="qty_in_stock" id="qty_in_stock" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Price Per Item</label>
                                        <input type="text" class="form-control" name="price_per_item" id="price_per_item" >
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                    <button class="btn btn-primary btn-sm" type="button" onclick="save()">Save</button>
                                    <button class="btn btn-warning btn-sm" type="button" onclick="reset()">Clear</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card " >
                    <div class="card-body">
                        <h4>Llst</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">product name</th>
                                    <th scope="col">qty in stock</th>
                                    <th scope="col">price per item</th>
                                    <th scope="col">Total Value Number</th>
                                    <!-- <th scope="col">Action</th> -->
                                </tr>
                            </thead>
                            <tbody id="product_table_body">
                                
                                
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
       
    </body>
    <script>
    
    $(document).on('ready', function()
    {
        fetch();
    })
        function save() {
                $.ajax({
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'{{ route("products.store") }}',
                data:$("#product_form").serializeArray(),
                success:function(data) {
                    if(data!==false)
                    {
                        fetch();
                        reset();
                        $("#success_message").html('Successfully saved');
                    }
                },
                error : function(error_data)
                {
                    var error_ht = '';
                    $.each(error_data.responseJSON['errors'], function(i,r){
                        error_ht += '<li >'+r+'</li>'
                        
                    });
                    $("#error_message").html(error_ht);
//                     
// 
                }
                });
            }
            function reset()
            {
                $("#product_name").val('');
                $("#qty_in_stock").val('');
                $("#price_per_item").val('');
                $("#success_message").empty();
                $("#error_message").empty();
            }
        function fetch() {
                $.ajax({
                type:'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'{{ route("products.index") }}',
                success:function(data) {
                    var product_html = '';
                    if(!$.isEmptyObject(data))
                    {
                        $.each(data, function(i,r)
                        {
                            product_html += '<tr>';
                            product_html += '<td>'+r['product_name']+'</td>';
                            product_html += '<td>'+r['qty_in_stock']+'</td>';
                            product_html += '<td>'+r['price_per_item']+'</td>';
                            product_html += '<td>'+( Number(r['price_per_item']) * Number(r['qty_in_stock']) ).toFixed(2)+'</td>';
                            // product_html += '<td></td>';
                            product_html += '</tr>';
                        });
                    }
                    $("#product_table_body").html(product_html);
                    
                }
                });
            }
    </script>
</html>
